<?php
add_filter('excerpt_more', 'new_excerpt_more');
add_theme_support('admin-bar');
add_theme_support('menus');
add_theme_support( 'post-thumbnails' ); 
add_image_size( 'post-homepage', 360, 240, true );
add_image_size( 'slide-homepage', 740, 493, true );
set_post_thumbnail_size( 360, 240 );
add_image_size( 'thumb-mobile', 576, 331, true );
add_action('init','register_menu');
add_action('init','create_custom_taxonomies');

// CUSTOM POST FORMAT
    function add_custom_post_format(){
        add_theme_support( 'post-formats', array( 'gallery','video' ));
    }
    add_action('init','add_custom_post_format');


function new_excerpt_more($more) {
  return "... <a href='".get_permalink($post->ID)."' class='more'>+More</a>";
}

/**
* defini um tamanho especifico para o excerpt
**/
function custom_excerpt_length( $length ) {
  return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/**
 * Add support for infinite scroll.
 */
add_theme_support( 'infinite-scroll', array(
    'type'           => 'scroll',
    'footer_widgets' => false,
    'container'      => 'conteudo',
    'wrapper'        => false,
    'render'         => false,
    'footer'		 => false,
    'posts_per_page' => get_option( 'posts_per_page' )
) );

//Remove logo from admin bar.
function theme_admin_adminbar_remove_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'wp_before_admin_bar_render', 'theme_admin_adminbar_remove_logo' );

// mostra barra do WP no topo do site somente quando o usuario estiver logado
function my_function_admin_bar(){ return (  is_user_logged_in() ) ? true : false; }


function register_menu(){
    if ( function_exists( 'register_nav_menus' ) ) {
        register_nav_menus(
            array(
              'mainMenu' => __('Main Menu'),
              'sidebarMenu' => __('Sidebar Menu')
            )
        );
    }
}

function creating_menu(){ wp_create_nav_menu( 'Main Menu', array( 'slug' => 'mainMenu' ) ); }

function create_custom_taxonomies()
{
    $labels = array(
        'name'              => _x( 'Section', 'taxonomy general name' ),
        'singular_name'     => _x( 'Section', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Sections' ),
        'all_items'         => __( 'All Sections' ),
        'parent_item'       => __( 'Parent Section' ),
        'parent_item_colon' => __( 'Parent Section:' ),
        'edit_item'         => __( 'Edit Section' ),
        'update_item'       => __( 'Update Section' ),
        'add_new_item'      => __( 'Add New Section' ),
        'new_item_name'     => __( 'New SectionName' ),
        'menu_name'         => __( 'Section' )
    );         

   register_taxonomy(
      'section', //nome da taxonomia
      'post', //nome do tipo de post que esta taxonomia vai ficar amarrada
      array(
         'label' => __( 'Section' ),
         'labels' => $labels,
         'rewrite' => array( 'slug' => 'section' ),
         'hierarchical' => true
      )
   );
}
?>
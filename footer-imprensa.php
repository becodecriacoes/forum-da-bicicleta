	<footer>
		<div class="container">
			<div id="patrocinadores">
				<img src="<?php bloginfo('template_url') ?>/images/patrocinadores.jpg">
			</div>
			
			<div class="criadores">
				<p>organizado pela <a href="" title="Ciclo Urbano">ong ciclo urbano</a></p>
				
				<div class="beco">
					<p>desenvolvido por</p>
					<figure>
						<a href="" class="beco-first"><img src="<?php bloginfo('template_url') ?>/images/beco-01.svg" alt="Beco de Criações"></a>
						<a href="" class="beco-second"><img src="<?php bloginfo('template_url') ?>/images/beco-02.svg" alt="Beco de Criações"></a>
					</figure>
				</div>
			</div>
		</div><!-- end .container -->
		
		
		<!-- scripts -->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		
		<script src="js/modernizr.custom.js"></script>
		<script src="js/jquery.min.js"></script>
		<script src='js/jquery.customSelect.js'></script>
		<script src='js/mootools-core-1.4.5.js'></script>
		<script src="js/jquery.colorbox.js"></script>
		
		<script>
			// saida caso o browser não aceite o formato svg
			if (!Modernizr.svg) {
				$("#menu .marca img").attr("src", "images/marca-menu.png");
				$("footer .beco figure .beco-first img").attr("src", "images/beco-01.png");
				$("footer .beco figure .beco-second img").attr("src", "images/beco-02.png");
			};
			
			// customização do select box
			$(document).ready(function(){
				$('select.styled').customSelect();
			});			
			
			$(function(){
				// carregamento ajax
				$("#carregando").hide();
				$(".lista-materias li h4").click(function(){
					pagina = $(this).attr('html')
				$("#carregando").ajaxStart(function(){
					$(this).show()
				})
				$("#carregando").ajaxStop(function(){
					$(this).hide();
				})	
				$(".imprensa .content .noticias article").load(pagina);
					return false;
				});
				
				// link de active na listagem de matérias
				$('.lista-materias li a').click(function(){
				  $('.lista-materias li').removeClass('active');
				  $(this).parent().addClass('active');
				});
			});
			
			// colorbox
			$(document).ready(function(){
				$(".inline").colorbox({inline:true, width:"35%", height:"67%"});
			});
            
            document.body.onclick = function (evt) {
				var node = evt.target;
					
				if(node.nodeName === "INPUT") {
					var isChecked = node.checked;
					(isChecked) ?
						isChecked = false :
					isChecked = true;
				}
			};
       
            $(document).ready(function(){                
                $('#inscricao_form').click(function(){
                    //console.log('enviando inscricao' + $("#form_inscricao_dias").val());
                    $("#form_inscricao_dias:checked").each(function( index ) {
                        console.log( index + ": " + $(this).text() );
                    });
    
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "<?php bloginfo('template_url') ?>/inscricao.php",
                        data: "seu_nome="+$("#seu_nome").val()
                        +"&seu_email="+ $("#seu_email").val()
                        +"&empresa="+ $("#empresa").val()
                        +"&form_inscricao_sabendo="+ $("#form_inscricao_sabendo").val()
                        +"&dias_27="+ $("#dias_27:checked").val()
                        +"&dias_28_manha="+ $("#dias_28_manha:checked").val()
                        +"&dias_28_tarde="+ $("#dias_28_tarde:checked").val()
                        +"&dias_29_escola="+ $("#dias_29_escola:checked").val()
                        +"&dias_29_passeio="+ $("#dias_29_passeio:checked").val()
                        ,
                        success: function(handler) {
                            console.log(handler);
                            alert("Seu cadastro foi enviado com sucesso, obrigado!");
                        }
                    });
                return false;
                });
            });
            
            $(document).ready(function(){                
                $('#contato_form').click(function(){
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "<?php bloginfo('template_url') ?>/contato.php",
                        data: "seu_nome="+$("#seu_nome").val()
                        +"&seu_email="+ $("#seu_email").val()
                        +"&assunto="+ $("#assunto").val()
                        +"&sua_mensagem="+ $("#sua_mensagem").val()
                        ,
                        success: function(handler) {
                            console.log(handler);
                            alert("Sua mensagem foi enviada com sucesso, obrigado!");
                        }
                    });
                return false;
                });
            });
		</script>
	</footer><!-- end footer -->
</body>
</html>
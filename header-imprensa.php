<!doctype html>
<html class="no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" />
	<meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=0.5, user-scalable=no">
	<meta name="robots" content="noindex, nofollow">
	
	<title>I Fórum Sergipano da Bicicleta - Cidades em movimento</title>


	<meta property='og:title' content='I Fórum Sergipano da Bicicleta' />
    <meta property='og:description' content='Cidades em movimento' />
    <meta property='og:url' content='http://www.amalerohigh.me' />
    <meta property='og:image' content='http://www.amalerohigh.me/logo-meta_tags.jpg'/>
    <meta property='og:type' content='hotsite' />
    <meta property='og:site_name' content='I Fórum Sergipano da Bicicleta organizado por Ciclo Urbano' />

    
	<link rel="shortcut icon" href="fav-icon.ico" type="image/x-icon"> 
	<link rel="icon" href="fav-icon.gif" type="image/gif">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link href="<?php bloginfo('template_url') ?>/style.css" rel="stylesheet" type="text/css" media="screen">
	<link href='http://fonts.googleapis.com/css?family=Roboto:100,400,300,500,300italic,400italic,500italic,700,700italic' rel='stylesheet' type='text/css'>
	
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/font-awesome-ie7.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/font-awesome.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/colorbox.css">
</head>
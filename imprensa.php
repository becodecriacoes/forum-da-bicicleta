<?php
/*
Template Name: Imprensa
*/
?>
<?php get_header('imprensa'); ?>
<body onload="initialize()" class="imprensa">
	<header id="inicio">
		<div class="container">
			<nav id="menu">
				<a href="/" class="marca"><img src="<?php bloginfo('template_url') ?>/images/marca-menu.svg"></a>
				
				<ul>
					<li><a href="/#o_evento" title="Sobre o evento">O Evento</a></li>
					<li><a href="/#palestrantes" title="Conheça os palestrantes">Palestrantes</a></li>
					<li><a href="/#programacao" title="Veja a programação">Programação</a></li>
					<li><a href="/#localizacao" title="Saiba como chegar">Como chegar</a></li>
					<li><a href="imprensa.php" title="Sala de imprensa">Imprensa</a></li>
					<li><a href="/#patrocinadores" title="Conheça os patrocinadores">Patrocinadores</a></li>
					<li><a href="/#contato" title="Entre em contato">Fale conosco</a></li>
				</ul>
			</nav><!-- end nav -->
		</div><!-- end .container -->
	</header><!-- end header -->

	<main class="content">
		<div class="container">
			<h2>Sala de Imprensa</h2>
			<br>
			
			<section class="release">
				<h3>Release</h3>
				<p>
					De 27 a 29 de setembro Aracaju ai sediar o 1º Fórum Sergipano da Bicicleta. O evento promovido pelo ONG Ciclo Urbano tem o objetivo de discutir a inclusão da bicicleta como meio de transporte sustentável em Sergipe.
<br><br>
O Fórum é uma oportunidade para reunir a sociedade civil, o poder público e a iniciativa privada durante os três dias do evento. No encontro acontecerá palestras e debates sobre temas relacionados a infra-estrutura urbana voltada para mobilidade sustentável no Brasil.
<br><br>
Ao longo do evento, vários temas estarão em pauta, entre eles: os principais problemas enfrentados por quem usa a bicicleta como meio de transporte, soluções para esses problemas, planejamento cicloviário, educação, políticas públicas, infra-estrutura urbana, meio ambiente, saúde coletiva e economia. 
<br><br>
Além de contar com a presença de grupos locais que compartilharão sua experiência, o evento contará também com palestrantes renomados no cenário de cicloativismo nacional.  Zé Lobo (Associação Transporte Ativo);  João Paulo (Bike Anjo);  Renata Falzoni  (ativista e vídeo-repórter da ESPN);  Altamirando Moraes (Sub- Secretário de Meio Ambiente do Rio de Janeiro); Waldson Costa (ONG Ciclo Urbano).
<br><br>
Durante o evento também será promovido um passeio ciclístico e o workshop Mão na Roda, onde os participantes aprenderam a desenvolver a manutenção de suas bicicletas.
<br><br>
1º Fórum Sergipano da Bicicleta é gratuito e para participar basta se inscrever no site do evento. Maiores informações e programação completa no site: http://www.fsbici.com.br/</p>
				
				<a href="../docs/release-fsbici.doc" title="Baixe o arquivo .doc" class="download-doc">baixe o arquivo .doc</a>
			</section><!-- end .release -->
			
			<section class="noticias">
				<h3>Notícias</h3>
				
				<ul class="lista-materias">
					<li class="active"><h4 value="http://forumdabicicleta.localhost/lol/" >Matéria para selecionar #01</h4></li>
					<li><h4 value="http://forumdabicicleta.localhost/lol/">Matéria para selecionar #02</h4></li>
					<li><a href="test-arquivo03.html">Matéria para selecionar #03</a></li>
					<li><a href="test-arquivo04.html">Matéria para selecionar #04</a></li>
				</ul><!-- end .lista-materias -->
				
				<article>
					<?php include("imprensa/test-arquivo01.html"); ?>
				</article>
			</section><!-- end .noticias -->
		</div><!-- end .container -->
	</main><!-- end main -->

	
	<section id="contato">
		<div class="container">
			<article>
				<form>
					<fieldset>
						<legend>Fale com a organização</legend>
						
						<input name="seu_nome" placeholder="Seu Nome" class="inputs">
						<input name="seu_email" placeholder="Seu Email" class="inputs">
						
						<select name="assunto" class="inputs styled">
							<option value="" selected disabled>Assunto</option>
							<option value="1">Item 1</option>
							<option value="2">Item 2</option>
							<option value="3">Item 3</option>
						</select>
						
						<textarea name="sua_mensagem" placeholder="Sua mensagem" class="inputs"></textarea>
						
						<button type="reset">Limpar campos</button>
						<button type="submit">Enviar</button>
					</fieldset>
				</form>
				
				<div class="column-right">
					<a href="#inline_content" class="inscrever inline" title="I Fórum Sergipano da Bicicleta">Inscreva-se</a>
					
					<div class="redes_sociais">
						<h3 name="Acompanhe o I Fórum Sergipano da Bicicleta nas redes sociais">Acompanhe nas redes sociais</h3>
						
						<div>
							<a href="https://www.facebook.com/fsbici" target="_blank" class="facebook"><span class="icon-facebook"></span></a>
							<a href="https://twitter.com/Ciclo_Urbano" class="twitter" target="_blank"><span class="icon-twitter"></span></a>
							<a href="http://instagram.com/ciclo_urbano" class="instagram" target="_blank"><span class="icon-instagram"></span></a>
							<a href="" class="youtube no-right" target="_blank"><span  class="icon-youtube-play"></span></a>
						</div>
					</div>
				</div>
			</article>
			
			<article style='display:none'>
				<div id="inline_content" style="padding:10px; background:#fff;">
					<form>
						<fieldset>
							<input name="seu_nome" placeholder="Seu Nome" class="inputs">
							<input name="seu_email" placeholder="Seu Email" class="inputs">
							<input name="empresa" placeholder="Empresa" class="inputs">
						</fieldset>
						
						<fieldset>
							<legend>Inscrever-se nos dias:</legend>
							<ul>
								<li><input type="checkbox" /> <label>Inscrever para dia 27 (Abertura/Palestras)</label></li>
								<li><input type="checkbox" /> <label>Inscrever para dia 28 – manhã (Palestras)</label></li>
								<li><input type="checkbox" /> <label>Inscrever para dia 28 – tarde (Palestras)</label></li>
								<li><input type="checkbox" /> <label>Inscrever para dia 29 -  (Escola Bike Anjo)</label></li>
								<li><input type="checkbox" /> <label>Inscrever para dia 29 – (Passeio Ciclístico)</label></li>
							</ul>
						</fieldset>
						
						<fieldset style="position: relative;">
							<legend>como soube do evento?</legend>
							
							<select name="sabendo" class="inputs styled">
								<option value="" selected disabled>Esconha uma opção</option>
								<option value="1">Site de notícias</option>
								<option value="2">TV</option>
								<option value="3">Busca pela internet</option>
								<option value="3">Mídias Sociais</option>
								<option value="3">Através de amigos</option>
								<option value="3">Outro</option>
							</select>
						</fieldset>
						
						<button type="submit">Inscrever</button>
					</form>
				</div><!-- end #inline_content -->
			</article>
			
			<hr>
			
		</div><!-- end .container -->
	</section><!-- end #programacao -->
	
<?php get_footer('imprensa'); ?>